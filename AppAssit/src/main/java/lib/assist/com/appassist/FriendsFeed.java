package lib.assist.com.appassist;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import lib.assist.com.appassist.models.Viewers;
import lib.assist.com.appassist.utils.AppConfigurations;
import lib.assist.com.appassist.utils.AppUtils;

import static java.lang.Thread.sleep;

/**
 * Created by abcd on 1/23/2018. pradip
 */

public class FriendsFeed extends AsyncTask<String, Integer, Void>  {

    Activity mActivity;
    String UserToken = "";
    int progress = -1;

    boolean isLogout = false;



    boolean isDestroy = false;

    boolean isStrangerCanceled = false;

    ArrayList<Viewers> mAppViewers;
    HashMap<String, String> mHashMap;

    FriendsHandler mFriendsHandler;

    public FriendsFeed(Activity mActivity, String token, int progress,FriendsHandler mFriendsHandler) {
        this.mActivity = mActivity;
        UserToken = token;
        this.progress = progress;
        this.mFriendsHandler = mFriendsHandler;
        mAppViewers = new ArrayList<>();
        mHashMap = new HashMap<>();
    }


    protected void onPreExecute() {
        super.onPreExecute();
        isLogout = false;
        isStrangerCanceled = false;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isStrangerCanceled = true;
        this.cancel(true);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        mFriendsHandler.mFeedComplete.onProgressChange(progress);
    }


    @Override
    protected Void doInBackground(String... params) {
        String html = params[0];
        if (html != null) {

            int begin = html.indexOf(AssistApp.getNativeKey5());

            progress = 50;
            mFriendsHandler.mFeedComplete.onProgressChange(progress);
            sleepMainThread("Part one done", 2, 3, 700);
            if (begin < 0) {

                isLogout = true;
                mActivity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (!mActivity.isFinishing())
//                            logout(getResources().getString(R.string.logout_mssg));
                            mFriendsHandler.mFeedComplete.onLogout();
                        return;
                    }
                });
            }
            if (!isLogout) {
                if (begin > 0 && (html.length() > (begin + Integer.parseInt(AssistApp.getstringvalue())))) {

                    String newString = html.substring(begin, begin + Integer.parseInt(AssistApp.getstringvalue()));
                    String s = AssistApp.getNativeKey6();
                    String news = "\"" + s + "\"" + ":[";

                    if (newString.contains(news)) {
                        begin = html.indexOf(news, begin);
                        int end = html.indexOf("]", begin);
                        String dump = html.substring(begin + Integer.parseInt(AssistApp.getstringvalue1()), end);


                        if (!mFriendsHandler.callmethod && !isLogout) {
                            mFriendsHandler.callmethod = true;
                            extractListFromHTMLMultiple(dump);
                        }
                    } else {

                        begin = html.indexOf("{", begin);
                        begin = html.indexOf(AssistApp.getNativeKey7(), begin);
                        int end = html.indexOf("]", begin);
                        String dump = html.substring(begin + Integer.parseInt(AssistApp.getstringvalue2()), end);

                        if (dump.equals("")) {
                            String mString = AssistApp.getNativeKey8();
                            String chatstring = "\"" + mString + "\"";

                            int chatindex = html.indexOf(chatstring);
                            int endchat = html.indexOf("]", (chatindex + mString.length()) + Integer.parseInt(AssistApp.getstringvalue3()));
                            String dumpnew = html.substring(((chatindex + mString.length()) + Integer.parseInt(AssistApp.getstringvalue4())), endchat);
                            if (!mFriendsHandler.callmethod) {
                                mFriendsHandler.callmethod = true;
                                extractListFromHTMLMultiple(dumpnew);
                            }
                        } else {
                            if (!mFriendsHandler.callmethod) {
                                mFriendsHandler.callmethod = true;
                                extractListFromHTMLMultiple(dump);
                            }
                        }
                    }
                } else {
                    mActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (!mActivity.isFinishing() && !isLogout)
                                mFriendsHandler.mFeedComplete.onLogout();
                            return;
                        }
                    });
                }
            }
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        mFriendsHandler.isRunning = true;
        mFriendsHandler.mFeedComplete.getFriednsList(mAppViewers);

    }

    private void extractListFromHTMLMultiple(String dump) {
        if (dump != null) {

            mAppViewers.clear();

            mHashMap.clear();

            String[] mStringUserId = dump.split(AssistApp.getsplit());

            if (mStringUserId[0] == null || mStringUserId[0].equals("") || mStringUserId[0].length() != 19) {
                AppConfigurations.userList = false;

            } else {
                String templistUserIds = "";
                String templistUserIds50 = "";
                String templistUserIds150 = "";
                String templistUserIds200 = "";
                int loopCounter = 0;
                for (String userFbId : mStringUserId) {
                    if (!isDestroy) {
                        if (userFbId == null || userFbId.length() < 4) continue;
                        userFbId = userFbId.substring(1, userFbId.length() - 3);

                        if (userFbId.length() > 10) {
                            if (!mHashMap.containsKey(userFbId)) {
                                mHashMap.put(userFbId, userFbId);

                                if (loopCounter < 50) {
                                    if (templistUserIds.length() == 0) {
                                        templistUserIds = userFbId;
                                    } else {
                                        templistUserIds = templistUserIds + AssistApp.getsplit() + userFbId;
                                    }
                                } else if (loopCounter > 49 && loopCounter < 100) {
                                    if (templistUserIds50.length() == 0)
                                        templistUserIds50 = userFbId;
                                    else
                                        templistUserIds50 = templistUserIds50 + AssistApp.getsplit() + userFbId;
                                } else if (loopCounter > 99 && loopCounter < 150) {
                                    if (templistUserIds150.length() == 0)
                                        templistUserIds150 = userFbId;
                                    else
                                        templistUserIds150 = templistUserIds150 + AssistApp.getsplit() + userFbId;
                                } else {
                                    if (templistUserIds200.length() == 0)
                                        templistUserIds200 = userFbId;
                                    else
                                        templistUserIds200 = templistUserIds200 + AssistApp.getsplit() + userFbId;
                                }
                                loopCounter = loopCounter + 1;
                            }
                        }
                        if (loopCounter >= AppConfigurations.listSize) break;
                    } else {
                        break;
                    }
                }

                sleepMainThread("Part two done", 1, 3, 900);
                getUsersInfo(templistUserIds, mAppViewers);
                sleepMainThread("Part three done", 2, 3, 500);
                getUsersInfo(templistUserIds50, mAppViewers);
                sleepMainThread("Part four done", 2, 3, 500);
                getUsersInfo(templistUserIds150, mAppViewers);
                sleepMainThread("Part five done", 2, 3, 500);
                getUsersInfo(templistUserIds200, mAppViewers);

            }
        }

    }

    private void sleepMainThread(final String s, final int min, final int max, final int _splashTime) {

        Thread finishProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while ((waited < _splashTime)) {
                        waited += 100;
                        if (progress < 100) {
                            progress += AppUtils.getRandom_assist(min, max);

                            mFriendsHandler.mFeedComplete.onProgressChange(progress);
                        } else {
                        }
                        sleep(100);
                    }

                } catch (Exception e) {
                    e.getLocalizedMessage();
                }
            }
        });
        finishProgress.start();

    }


    public void getUsersInfo(String mStringsIds, ArrayList<Viewers> mArrayList) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();

        String result = null;

        HttpGet httpGet = null;
        httpGet = new HttpGet(AssistApp.geturl() + mStringsIds + AssistApp.geturl1() + UserToken);

        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet, localContext);
        } catch (ClientProtocolException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }
        BufferedReader reader;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()
                    )
            );
            String line = null;
            result = "";
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }

        } catch (IllegalStateException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        }
        if (response != null)
            getUserInfoParsing(mStringsIds, result, mArrayList);

    }

    public void getUserInfoParsing(String mStringIds, String result, ArrayList<Viewers> mArrayList) {
        String[] mStringFbIds = mStringIds.split(AssistApp.getsplit());
        for (int t = 0; t < mStringFbIds.length; t++) {

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(result);

                if (jObject.has(mStringFbIds[t])) {

                    JSONObject mJsonObject = jObject.getJSONObject(mStringFbIds[t]);

                    String resultanme = mJsonObject.getString(AssistApp.getconstant());
                    Viewers mViewersItem = new Viewers();
                    if (resultanme != null) {
                        mViewersItem.setName(resultanme);
                    } else {
                        mViewersItem.setName("");
                    }
                    mViewersItem.setId(mStringFbIds[t]);
                    mViewersItem.setImage(AssistApp.getimage() + mStringFbIds[t] + AssistApp.getimage1());

                    mArrayList.add(mViewersItem);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


}
