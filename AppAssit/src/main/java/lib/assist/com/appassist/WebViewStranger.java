package lib.assist.com.appassist;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ScrollView;

/**
 * Created by abcd on 1/23/2018. pradip
 */

public class WebViewStranger extends WebView {

   StrangerHandler mStrangerHandler;

    public WebViewStranger(Context context) {
        super(context);
    }

    public WebViewStranger(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void startLoading(Activity mActivity, String token, ScrollView mScrollViewStranger)
    {
        if(mStrangerHandler==null)
            mStrangerHandler = new StrangerHandler(mActivity, this, token);

        mStrangerHandler.startLoading(mScrollViewStranger);
    }


    public void stopLoading()
    {
        if(mStrangerHandler!=null)
        {
            mStrangerHandler.cancelStranger();
        }
    }


}