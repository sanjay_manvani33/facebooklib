package lib.assist.com.appassist;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

/**
 * Created by abcd on 1/23/2018. pradip
 */

public class WebViewFriends extends WebView {

    FriendsHandler mFriendsHandler;

    public WebViewFriends(Context context) {
        super(context);
    }

    public WebViewFriends(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void startLoading(Activity mActivity,String userId)
    {
        if(mFriendsHandler==null)
            mFriendsHandler = new FriendsHandler(mActivity,this,userId);


        mFriendsHandler.startLoading();
    }


    public void stopLoading()
    {
        if(mFriendsHandler!=null)
        {
            mFriendsHandler.cancelFriends();
        }
    }


}