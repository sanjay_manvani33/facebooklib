package lib.assist.com.appassist;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import lib.assist.com.appassist.models.Viewers;
import lib.assist.com.appassist.utils.AppUtils;

/**
 * Created by abcd on 1/23/2018. pradip
 */

public class FriendsHandler  {


    public interface OnFriendsFeedComplete {

        void getFriednsList(ArrayList<Viewers> mAppViewers);

        void onLogout();

        void onProgressChange(int progress);
    }


    public  OnFriendsFeedComplete mFeedComplete;



    Activity mActivity;
    String token = "";

    WebView mWebViewFacebook;
    FriendsFeed mFriendsFeed;

    boolean webUsers = false;
    boolean isFacebookCalling = false;

     boolean callmethod = false;
     boolean isRunning = false;

    int progress = 3;

    public FriendsHandler(Activity mActivity, WebView mWebView, String userId) {

        mFeedComplete = (OnFriendsFeedComplete) mActivity;
        mWebViewFacebook = mWebView;
        this.mActivity = mActivity;
        token = userId;
    }

    public void startLoading() {

        webUsers = false;
        isRunning = true;
        isFacebookCalling = false;
        callmethod = false;
        if(mFriendsFeed==null)
            mFriendsFeed = new FriendsFeed(mActivity, token, progress,this);


        if (isFriendsRunning()) {
            cancelFriends();
        }

        AppUtils.setWebSettings_assist(mWebViewFacebook.getSettings());
        mWebViewFacebook.setWebViewClient(new Callback());
        mWebViewFacebook.addJavascriptInterface(new MyJavaScriptInterface(), AssistApp.getNativeKey3());
        webData();
    }

    public void webData() {

        isFacebookCalling = false;
        isRunning = true;
        mFeedComplete.onProgressChange(progress);
        mWebViewFacebook.loadUrl(AssistApp.getNativeKey1());

    }

    private class Callback extends WebViewClient {

        Timer mTimer = new Timer();
        int progress = 5;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);


            if (!isFacebookCalling) {
                progress = 5;
                mTimer = new Timer();
                isFacebookCalling = true;
                mFeedComplete.onProgressChange(progress);

                mTimer.scheduleAtFixedRate(new TimerTask() {

                    @Override
                    public void run() {

                        mActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (progress < 40 && !callmethod) {
                                    progress += 3;
                                    mFeedComplete.onProgressChange(progress);
                                } else {
                                    mTimer.cancel();
                                }
                            }
                        });
                    }
                }, 1000, 1200);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mTimer.cancel();
            progress = 40;
            mFeedComplete.onProgressChange(progress);

            if (!webUsers) {
                webUsers = true;

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isRunning) {
                            isRunning = false;
                            mWebViewFacebook.loadUrl(AssistApp.finalkey());

                        }
                    }
                });

            }
        }
    }

    public void refreshData()
    {



    }

    class MyJavaScriptInterface {

        boolean isprocessed = false;

        @JavascriptInterface
        public void processHTML(String html) {
            if (isprocessed)
                return;
            progress = 47;
            mFeedComplete.onProgressChange(progress);

            mFriendsFeed = new FriendsFeed(mActivity, token, progress,FriendsHandler.this);
            mFriendsFeed.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, html);

        }
    }


    public boolean isFriendsRunning() {
        if (mFriendsFeed.getStatus() == AsyncTask.Status.RUNNING) {
            return true;
        } else {
            return false;
        }
    }

    public void cancelFriends() {
        if (mFriendsFeed != null)
            mFriendsFeed.cancel(true);
    }

}