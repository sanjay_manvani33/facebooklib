package lib.assist.com.appassist;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import lib.assist.com.appassist.models.Viewers;
import lib.assist.com.appassist.utils.AppUtils;

import static java.lang.Thread.sleep;
/**
 * Created by abcd on 1/19/2018. pradip
 */

public class StrangersFeed extends AsyncTask<String, Integer, Void> {


    Activity mActivity;
    String UserToken = "";
    int progress = -1;

    boolean isLogout = false;


    boolean isRunningStranger = false;
    boolean callstrangers = false;
    boolean isStrangerCanceled = false;
    boolean isDestroy = false;
    boolean isprocessed = false;

    ArrayList<Viewers> mAppViewersPeople;



    public StrangersFeed(Activity mActivity, String token, int progress) {
        this.mActivity = mActivity;
        UserToken = token;
        this.progress = progress;

        mAppViewersPeople = new ArrayList<>();
    }

    protected void onPreExecute() {
        super.onPreExecute();
        isLogout = false;
        isStrangerCanceled = false;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isStrangerCanceled = true;
        Log.e("StrangersFeed.", "StrangersFeed.oncancelled called.");
        this.cancel(true);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
//        mActivity.setProgress(100*integer);
        StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
    }

    protected Void doInBackground(String... params) {
        System.out.println("isStrangerCanceled: " + isStrangerCanceled);

        String html = params[0];
        if (html != null) {

            isprocessed = true;
            int begin = html.indexOf(AssistApp.getNativeKey9());
            boolean flag = false;

            if (begin < 0) {
                isLogout = true;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!mActivity.isFinishing())
                            //logout(getResources().getString(R.string.logout_mssg));
                            StrangerHandler.mFeedComplete.onStrangersLogout();
                        return;
                    }
                });
            }
            String tempname = "";
            String tempname50 = "";

            if (begin > 0) {
                mAppViewersPeople.clear();
            }

            if (!isLogout) {
                if (begin > 0) {

                    int count = 0;
                    do {
                        if (begin > 0 && html.length() > begin) {
                            html = html.substring(begin);
                            int end1 = 0;
                            end1 = html.indexOf(AssistApp.getNativeKey10());
                            String id = html.substring(0, end1);

                            int start = id.indexOf(AssistApp.getNativeKey11());
                            String newid = id.substring(start + Integer.parseInt(AssistApp.getstringvalue6()), start + Integer.parseInt(AssistApp.getstringvalue7()));

                            start = newid.indexOf(" ");
                            newid = newid.substring(0, start - 1);

                            html = html.substring(end1);
                            begin = 0;
                            begin = html.indexOf(AssistApp.getNativeKey9());

                            if (count < 50) {

                                if (newid.matches(AssistApp.numbers())) {
                                    if (tempname.length() == 0)
                                        tempname = newid;
                                    else
                                        tempname += AssistApp.getsplit() + newid;

                                }
                            } else if (count < 100) {
                                if (newid.matches(AssistApp.numbers())) {
                                    if (tempname50.length() == 0)
                                        tempname50 = newid;
                                    else
                                        tempname50 += AssistApp.getsplit() + newid;
                                }
                            }

                            if (newid.matches(AssistApp.numbers())) {
                                count = count + 1;
                            }
                        }
                        if (begin < 0)
                            flag = true;

                        if (count > 99)
                            flag = true;

                    } while (!flag);

                }

                progress = 50;
                StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
                if (!isStrangerCanceled) {
                    try {
//                        setProgressBar(progress);
                        if (!callstrangers && !isLogout) {
                            callstrangers = true;

                            sleepMainThread("Part one done", 2, 3, 700);
                           // StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
                            getUsersInfo(tempname, mAppViewersPeople);

                            sleepMainThread("Part two done", 1, 3, 900);
                           // StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
                            getUsersInfo(tempname50, mAppViewersPeople);

                            sleepMainThread("Part three done", 2, 3, 500);
//                            strangerList(mAppViewersPeople);

                            sleepMainThread("Part last", 2, 3, 100);
//                            setStrangerDataList();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        isRunningStranger = true;
        StrangerHandler.mFeedComplete.getStrangersList(mAppViewersPeople);
    }

    /*
    ---------------------------------------------------------------------------
     */

    private void sleepMainThread(final String s, final int min, final int max, final int _splashTime) {

        Thread finishProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while ((waited < _splashTime)) {
                        waited += 100;
                        if (progress < 100) {
                            progress += AppUtils.getRandom_assist(min, max);
//                            setProgressBar(progress);
                            StrangerHandler.mFeedComplete.onStrangersProgressChange(progress);
                        } else {
//                            setProgressBar(99);
                        }
                        System.out.println("sleepMainThread string: " + s + " progress: " + progress);
                        sleep(100);
                    }

//                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.getLocalizedMessage();
                }
            }
        });
        finishProgress.start();

//        StrangerList.this.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
////                    int _splashTime = 700;
//                    while ((waited < _splashTime)) {
//                        sleep(100);
////                                        if (_active)
//                        {
//                            waited += 100;
//                            if (progress < 100) {
//                                progress += AppUtils.getRandom_assist(min, max);
//                                setProgressBar(progress);
//                            } else {
//                                setProgressBar(99);
//                            }
//                            System.out.println("sleepMainThread s: " + s + " progress: " + progress);
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    public void getUsersInfo(String mStringsIds, ArrayList<Viewers> mArrayList) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();

        String result = null;

        HttpGet httpGet = null;
        String url = AssistApp.geturl() + mStringsIds + AssistApp.geturl1() + UserToken;
        System.out.println("getUsersInfo url: " + url);
        httpGet = new HttpGet(url);

        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet, localContext);
        } catch (ClientProtocolException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = null;
            result = "";
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }

            System.out.println("getUsersInfo result: " + result);
        } catch (IllegalStateException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        }

        if (response != null)
            getUserInfoParsing(mStringsIds, result, mArrayList);

    }

    public void getUserInfoParsing(String mStringIds, String result, ArrayList<Viewers> mArrayList) {
        String[] mStringFbIds = mStringIds.split(AssistApp.getsplit());
        for (int t = 0; t < mStringFbIds.length; t++) {

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(result);

                if (jObject.has(mStringFbIds[t])) {

                    JSONObject mJsonObject = jObject.getJSONObject(mStringFbIds[t]);

                    String resultanme = mJsonObject.getString(AssistApp.getconstant());
                    Viewers mAppViewersItem = new Viewers();

//                    System.out.println("StrangersFeed.getUserInfoParsing()  resultanme: " + resultanme);
                    if (resultanme != null) {
                        mAppViewersItem.setName(resultanme);
                        if (resultanme.contains(" ") && resultanme.length() > 0)
                            mAppViewersItem.setFirstname(resultanme.substring(0, resultanme.indexOf(" ")));
                        else
                            mAppViewersItem.setFirstname(resultanme);
                    } else {
                        mAppViewersItem.setName("");
                    }
                    mAppViewersItem.setId(mStringFbIds[t]);
                    mAppViewersItem.setImage(AssistApp.getimage() + mStringFbIds[t] + AssistApp.getimage1());

                    mArrayList.add(mAppViewersItem);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

}
