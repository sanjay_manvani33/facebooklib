#include <jni.h>

    /* -------------------------------------------------------------------------------
                                        StrangerList
       -------------------------------------------------------------------------------
    */

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey1(JNIEnv *env, jobject instance) {
        return(*env)->  NewStringUTF(env, "https://facebook.com");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey2(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "TmF0aXZlNWVjcmV0UEBzc3cwcmQy");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey3(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "HTMLOUT");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey4(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "https://www.facebook.com/friends/requests");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey5(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "InitialChatFriendsList");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey6(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "list");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey7(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "list:[");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey8(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "ChatSidebarInitialDataPreloader");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey9(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "<li class=\"friendBrowserListUnit\"");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey10(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "</li>");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey11(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "data-profileid");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getNativeKey12(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "</a> and");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getimage(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "https://graph.facebook.com/");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getimage1(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "/picture?type=large");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_geturl(JNIEnv *env, jobject instance) {
        return(*env)->NewStringUTF(env, "https://graph.facebook.com/?ids=");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_geturl1(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "&access_token=");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "200");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue1(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "8");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue2(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "6");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue3(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "26");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue4(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "28");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue5(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "28");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue6(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "16");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getstringvalue7(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "40");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getsplit(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, ",");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getconstant(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "name");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_register(JNIEnv *env, jobject instance) {
//        return (*env)->NewStringUTF(env, "http://thesuperapps.com/com.stalkersstranger.app/gcm.php?do=add_user&email=");
        return (*env)->NewStringUTF(env, "http://thesuperapps.com/com.stalkersstranger.app/gcm.php");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_adduser(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "add_user");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_uniqueid(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "face_id");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_setgcm(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "gcm_id");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_mainweb(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_common(JNIEnv *env, jobject instance) {
//        return (*env)->NewStringUTF(env, "http://thesuperapps.com/com.stalkersstranger.app/action.php?do=update_share&face_id=");
        return (*env)->NewStringUTF(env, "http://thesuperapps.com/com.stalkersstranger.app/action.php");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_commonrate(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "update_rate");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_commonshare(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "update_share");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_upmail(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "email");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_upd(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "id");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getperm1(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "email,public_profile");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getprm1(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "id,email,name");
    }
    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_getgrpReq(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "/me/permissions/");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_finalkey(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "javascript:window.HTMLOUT.processHTML('<html>'+document.body.innerHTML+'</html>');");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_sign1(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, ">");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_sign2(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "<");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_image3(JNIEnv *env, jobject instance) {
        return (*env)->NewStringUTF(env, "&height=300&width=300");
    }

    JNIEXPORT jstring JNICALL
    Java_lib_com_fbassist_AssistApp_numbers(JNIEnv *env, jobject instance) {
        return  (*env)->NewStringUTF(env, "^[0-9]*$");
    }
JNIEXPORT jstring JNICALL
Java_lib_com_fbassist_AssistApp_mainPhoto(JNIEnv *env, jobject instance) {
    return (*env)->NewStringUTF(env, "https://graph.facebook.com/");
}

JNIEXPORT jstring JNICALL
Java_lib_com_fbassist_AssistApp_mainPhototoken(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "?fields=cover&access_token=");
}
JNIEXPORT jstring JNICALL
Java_lib_com_fbassist_AssistApp_main1(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "cover");
}
JNIEXPORT jstring JNICALL
Java_lib_com_fbassist_AssistApp_resource(JNIEnv *env, jobject instance) {

    return (*env)->NewStringUTF(env, "source");
}
